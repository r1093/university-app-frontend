import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from 'src/app/Model/Course';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-get-all-courses',
  templateUrl: './get-all-courses.component.html',
  styleUrls: ['./get-all-courses.component.css']
})
export class GetAllCoursesComponent implements OnInit {

  courses:Course[];

  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
    this.service.getAllCourses()
    .subscribe(data => this.courses = data);
  }

}
