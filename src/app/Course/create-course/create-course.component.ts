import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from 'src/app/Model/Course';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {

  course:Course;

  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
  }

  save(courseName:String) {
    this.course = new Course();
    this.course.description = courseName;
    this.service.createCourse(this.course).subscribe(data => {
      alert("The course has been created succesfully");
      this.router.navigate(["get-all-courses"]);
    });
  }

}
