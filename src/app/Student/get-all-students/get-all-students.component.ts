import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from 'src/app/Model/Student';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-get-all-students',
  templateUrl: './get-all-students.component.html',
  styleUrls: ['./get-all-students.component.css']
})
export class GetAllStudentsComponent implements OnInit {

  students:Student[];

  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
    this.service.getAllStudents()
    .subscribe(data => this.students = data);
  }

  redirectEdit(studentId:number) {
    this.router.navigate(["edit-student"], {queryParams: {studentId: studentId}});
  }

}
