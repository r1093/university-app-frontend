import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/Model/Course';
import { Student } from 'src/app/Model/Student';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

  id:number;
  student:Student;

  studentCourses:Course[];
  availableCourses:Course[];
  addCoursesList:Course[];

  constructor(private service:ServiceService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(data => {
      this.id = Number(data.get('studentId'));
    });
    this.service.getStudentById(this.id).subscribe(data => {
      this.student = data;
      (<HTMLInputElement>document.getElementById('name')).value = String(this.student.name);
      (<HTMLInputElement>document.getElementById('lastName')).value = String(this.student.lastName);
    });
    this.service.getAllCoursesByStudentId(this.id).subscribe(data => this.studentCourses = data);
    this.service.getAllCourses().subscribe(data => {
      this.availableCourses = data;
      for (let i = 0; i < this.availableCourses.length; i++) {
        for (let j = 0; j < this.studentCourses.length; j++) {
          if (this.availableCourses[i].id == this.studentCourses[j].id) {
            this.availableCourses.splice(i, 1);
          }
        }
      }
    });
  }

  addCourse(course:Course) {
    this.service.addStudentToCourse(this.id, course.id).subscribe(data => {});
    this.availableCourses = this.availableCourses.filter(aCourse => aCourse != course);
    this.studentCourses.push(course);
  }

  removeCourse(course:Course) {
    this.service.removeStudentFromCourse(this.id, course.id).subscribe(data => {});
    this.studentCourses = this.studentCourses.filter(aCourse => aCourse != course);
    this.availableCourses.push(course);
  }

}
