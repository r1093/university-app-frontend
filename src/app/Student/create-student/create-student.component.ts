import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from 'src/app/Model/Student';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  student:Student;

  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
  }

  save(name:String, lastName:String) {
    this.student = new Student();
    this.student.name = name;
    this.student.lastName = lastName;

    this.service.createStudent(this.student)
    .subscribe(data => {
      alert("The student has been created succesfully");
      this.router.navigate(["get-all-students"]);
    });
  }

}
