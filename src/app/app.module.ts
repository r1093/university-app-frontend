import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateStudentComponent } from './Student/create-student/create-student.component';
import { GetAllStudentsComponent } from './Student/get-all-students/get-all-students.component';
import { EditStudentComponent } from './Student/edit-student/edit-student.component';
import { GetAllCoursesComponent } from './Course/get-all-courses/get-all-courses.component';
import { FormsModule } from '@angular/forms';
import { ServiceService } from '../app/Service/service.service';
import { HttpClientModule } from '@angular/common/http';
import { CreateCourseComponent } from './Course/create-course/create-course.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateStudentComponent,
    GetAllStudentsComponent,
    EditStudentComponent,
    GetAllCoursesComponent,
    CreateCourseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
