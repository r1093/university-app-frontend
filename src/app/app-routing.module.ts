import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCourseComponent } from './Course/create-course/create-course.component';
import { GetAllCoursesComponent } from './Course/get-all-courses/get-all-courses.component';
import { CreateStudentComponent } from './Student/create-student/create-student.component';
import { EditStudentComponent } from './Student/edit-student/edit-student.component';
import { GetAllStudentsComponent } from './Student/get-all-students/get-all-students.component';

const routes: Routes = [
  {path:'create-student', component:CreateStudentComponent},
  {path:'get-all-students', component:GetAllStudentsComponent},
  {path:'edit-student', component:EditStudentComponent},
  {path:'get-all-courses', component:GetAllCoursesComponent},
  {path:'create-course', component:CreateCourseComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
