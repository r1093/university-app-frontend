import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Student } from '../Model/Student';
import { Course } from '../Model/Course';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  Url=['http://localhost:8001/students', 'http://localhost:8001/courses'];

  createStudent(student:Student) {
    return this.http.post<Student>(this.Url[0] + "/create", student);
  }

  getAllStudents() {
    return this.http.get<Student[]>(this.Url[0] + "/get-all")
  }

  getStudentById(id:number) {
    return this.http.get<Student>(this.Url[0] + "/get-by-id", 
    {params: {id: id}});
  }

  addStudentToCourse(studentId:number, courseId:number) {
    return this.http.get<void>(this.Url[0] + "/add-students-to-course", 
    {params: {studentId: studentId, courseId: courseId}});
  }

  removeStudentFromCourse(studentId:number, courseId:number) {
    return this.http.delete<void>(this.Url[0] + "/remove-students-from-course", 
    {params: {studentId: studentId, courseId: courseId}})
  }

  createCourse(course:Course) {
    return this.http.post<Course>(this.Url[1] + "/create", course);
  }

  getAllCourses() {
    return this.http.get<Course[]>(this.Url[1] + "/get-all")
  }

  getAllCoursesByStudentId(studentId:number) {
    return this.http.get<Course[]>(this.Url[1] + "/get-all-by-student-id", 
    {params: {studentId: studentId}})
  }
}
