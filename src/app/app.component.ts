import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'university-front';

  constructor(private router:Router) {}

  ngOnInit(): void {
    this.getAllStudents();
  }

  createStudent() {
    this.router.navigate(["create-student"]);
  }

  getAllStudents() {
    this.router.navigate(["get-all-students"]);
  }

  editStudent() {
    this.router.navigate(["edit-student"]);
  }

  createCourse() {
    this.router.navigate(["create-course"]);
  }

  getAllCourses() {
    this.router.navigate(["get-all-courses"]);
  }
}
